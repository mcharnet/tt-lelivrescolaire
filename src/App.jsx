import React, { useState } from 'react';
import { ThemeProvider } from 'emotion-theming';
import './App.css';
import styled from '@emotion/styled';
import Uuid from 'uuid/v4';
import data from './data/pupils';
import Classroom from './components/classroom.component';
import * as theme from './style/theme';
import PupilFormModal from './components/pupilFormModal.component';
import ToggleContent from './components/common/ToggleContent.component';

import Button from './components/common/Button.component';

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  padding: 5rem;
`;

function App() {
  const [pupils, setPupils] = useState(data);

  const addPupil = (pupil) => {
    const newPupil = {
      ...pupil,
      id: Uuid(),
    };
    setPupils([...pupils, newPupil]);
  };

  const removePupil = (pupil) => {
    const newPupils = pupils.filter(el => el !== pupil);
    setPupils(newPupils);
  };

  const editPupil = (editedPupil) => {
    const newPupils = pupils.map(x => (x.id === editedPupil.id ? editedPupil : x));
    setPupils(newPupils);
  };

  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
        <ToggleContent
          toggle={show => (
            <Button onClick={show}>Ajouter un élève</Button>
          )}
          content={hide => (
            <PupilFormModal
              title="Ajouter un élève"
              onSubmit={(pupil) => { addPupil(pupil); hide(); }}
              onClose={hide}
            />
          )}
        />

        <Classroom
          pupils={pupils}
          editPupil={pupil => editPupil(pupil)}
          removePupil={removePupil}
        />

      </Wrapper>

    </ThemeProvider>
  );
}

export default App;
