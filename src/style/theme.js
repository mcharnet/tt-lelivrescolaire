import colors from './colors';


export const globalStyle = {
  fontSize: '100%',
  lineHeight: '1.4',
  fontFamily: 'Avenir, Helvetica, Roboto, Arial',
  fontFamilyHeading: 'ITC Avant Garde, Avenir, Helvetica, Roboto, Arial',
  color: `${colors.primary.default}`,
  radius: '8px',
};


export const color = colors;
