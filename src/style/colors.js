const colors = {
  white: '#FFF',
  black: '#000000',
  primary: {
    lighter: '#F5FAFF',
    light: '#65A0D2',
    dark: '#12334e',
    default: '#19486E',
  },
  secondary: {
    light: '#bcb18f',
    default: '#907d45',
    dark: '#564b29',
  },
  grey: {
    lighter: '#F9FCFF',
    light: '#C9D1E3',
    default: '#888888',
  },
  callout: {
    success: '#2D937F',
    warning: '#F6962C',
    alert: '#E83239',
    info: '#CAEBFF',
    infoDark: '#88C9F0',
    error: '#dfccd6',
    errorDark: '#ac7c97',
  },
};

export default colors;
