import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';


export const InputStyled = styled.input`
  border: 1px solid ${props => props.theme.color.primary.default};
  display: block;
  width: 100%;
  box-sizing: border-box;
  height: 2rem;
  border-radius: 4px;
`;

const Label = styled('label')`
  display: block;
  margin-bottom: 0.8rem;
  color: ${props => props.theme.color.primary.default};
  font-size: 1.1rem;
  /* font-weight: 800; */
  cursor: pointer;

  &:first-letter {
    text-transform: uppercase;
  }
`;

const Wrapper = styled('div')`
`;

const Input = ({ label, id, onChange, name, value }) => (
  <Wrapper>
    {label ? <Label htmlFor={id}>{label}</Label> : null}
    <div>
      <InputStyled value={value} id={id} onChange={onChange} type="text" name={name} />
    </div>
  </Wrapper>

);

export default Input;


Input.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
};

Input.defaultProps = {
  label: null,
  id: null,
  value: null,
};
