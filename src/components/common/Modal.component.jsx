import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';


const ModalWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: 10;
  box-sizing: border-box;
`;

const ModalShade = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.3);
  cursor: pointer;
`;

const ModalCard = styled.div`
  position: relative;
  padding: 2rem 2rem 4.5rem;
  max-height: 100%;
  background: ${props => props.theme.color.secondary.light};
  overflow-y: auto;
  box-sizing: border-box;
`;

const ModalTitle = styled.h1`
  display: block;
  margin-bottom: 2rem;
  text-transform: uppercase;
  color: ${props => props.theme.color.primary.dark};
`;

const modalDOMNode = document.querySelector('#modal-root');

const Modal = ({
  isOpen,
  title,
  portalRef,
  children,
  onClose,
  disableEscapeKey,
  ...otherProps
}) => isOpen && ReactDOM.createPortal(
  (
    <ModalWrapper {...otherProps}>
      <ModalShade onClick={onClose} />
      <ModalCard>
        {title && <ModalTitle variant="headline">{title}</ModalTitle>}
        {children}
      </ModalCard>
    </ModalWrapper>
  ),
  portalRef,
);

Modal.propTypes = {
  isOpen: PropTypes.bool,
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  portalRef: PropTypes.object,
};

Modal.defaultProps = {
  isOpen: false,
  title: null,
  portalRef: modalDOMNode,
  onClose() {},
};

export default Modal;
