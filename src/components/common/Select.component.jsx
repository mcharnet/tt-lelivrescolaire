import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';


export const SelectStyled = styled.select`
  border: 1px solid ${props => props.theme.color.primary.default};
  display: block;
  width: 100%;
  height: 2rem;
`;

const Label = styled('label')`
  display: block;
  margin-bottom: 0.8rem;
  color: ${props => props.theme.color.primary.default};
  font-size: 1.2rem;
  font-weight: 800;
  cursor: pointer;

  &:first-letter {
    text-transform: uppercase;
  }
`;

const Wrapper = styled('div')`
`;

const Select = ({ label, id, onChange, name, options, value }) => (
  <Wrapper>
    {label ? <Label htmlFor={id}>{label}</Label> : null}
    <div>
      <SelectStyled id={id} onChange={onChange} name={name} value={value}>
        {options.map((option, i) => <option key={`${option.key + i}`} value={option.key}>{option.label}</option>)}
      </SelectStyled>
    </div>
  </Wrapper>

);

export default Select;


Select.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape).isRequired,
  value: PropTypes.string,
};

Select.defaultProps = {
  label: null,
  id: null,
  value: null,
};
