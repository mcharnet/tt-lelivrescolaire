import React from 'react';
import { shallow } from 'enzyme';

import Input from '../Input.component';

describe('Input', () => {
  let props;

  beforeEach(() => {
    props = {
      onChange: jest.fn(),
      name: 'name',
    };
  });

  const getWrapper = () => shallow(<Input {...props} />);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with specified props', () => {
    // Given
    props.label = 'Label';

    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
