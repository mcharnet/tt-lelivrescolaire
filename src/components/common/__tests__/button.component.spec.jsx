import React from 'react';
import { shallow } from 'enzyme';

import Button from '../Button.component';

describe('Button', () => {
  let props;

  beforeEach(() => {
    props = {
      onClick: jest.fn(),
    };
  });

  const getWrapper = () => shallow(<Button {...props}>Click</Button>);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with specified props', () => {
    // Given
    props.isStroked = true;
    props.onClick = jest.fn();

    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
