import React from 'react';
import { shallow } from 'enzyme';

import ToggleContent from '../ToggleContent.component';

describe('ToggleContent', () => {
  let props;

  beforeEach(() => {
    props = {
      toggle: jest.fn(),
      content: jest.fn(),
    };
  });

  const getWrapper = () => shallow(<ToggleContent {...props} />);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
