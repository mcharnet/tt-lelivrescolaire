import React from 'react';
import { shallow } from 'enzyme';

import Modal from '../Modal.component';

describe('Modal', () => {
  let props;

  beforeEach(() => {
    props = {
      title: 'Foo',
      isOpen: true,
    };
  });

  const getWrapper = () => shallow(<Modal {...props}>Modal</Modal>);

  it('should render properly', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
