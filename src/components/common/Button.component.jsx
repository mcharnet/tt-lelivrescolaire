import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';


export const ButtonStyled = styled.button`
  background-color: ${props => (props.isStroked ? props.theme.color.white : props.theme.color.primary.default)};
  color: ${props => (props.isStroked ? props.theme.color.primary.default : props.theme.color.white)};
  padding: 0.4rem 2rem;
  border-radius: ${props => props.theme.globalStyle.radius};
  cursor: pointer;
  font-size: 1rem;
  border: ${props => (props.isStroked ? `1px solid ${props.theme.color.primary.default}` : 0)};
  
  &:hover:enabled,
  &:active:enabled,
  &:focus:enabled {
    outline: none;
    color: ${props => props.theme.color.white};
    background-color: ${props => (props.isStroked ? props.theme.color.primary.light : props.theme.color.primary.dark)};
  }
`;

const Button = ({ className, children, type, isStroked, onClick }) => (
  <ButtonStyled className={className} type={type} isStroked={isStroked} onClick={onClick}>
    {children}
  </ButtonStyled>
);

export default Button;


Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  type: PropTypes.string,
  isStroked: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
};
Button.defaultProps = {
  children: null,
  type: 'button',
  isStroked: false,
  onClick: Function.prototype,
  className: null,
};
