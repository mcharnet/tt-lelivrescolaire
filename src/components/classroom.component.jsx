import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import PupilCard from './pupil.component';
import { pupilPropTypes } from './propTypes';

const Pupils = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  grid-gap: 2rem;
  width: 100%;
  padding: 2rem;  
`;


const Classroom = ({ pupils, editPupil, removePupil }) => (
  <Pupils>
    {pupils.map(pupil => (
      <PupilCard
        pupil={pupil}
        editPupil={editPupil}
        handleEdit={editPupil}
        removePupil={removePupil}
        key={pupil.id}
      />
    ))}
  </Pupils>


);

export default Classroom;

Classroom.propTypes = {
  pupils: PropTypes.arrayOf(PropTypes.shape(pupilPropTypes)).isRequired,
  editPupil: PropTypes.func.isRequired,
  removePupil: PropTypes.func.isRequired,
};

Classroom.defaultProps = {
};
