import PropTypes from 'prop-types';

// eslint-disable-next-line import/prefer-default-export
export const pupilPropTypes = {
  firstname: PropTypes.string.isRequired,
  lastname: PropTypes.string.isRequired,
  gender: PropTypes.string.isRequired,
};
