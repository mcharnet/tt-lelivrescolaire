import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import male from '../assets/male.png';
import female from '../assets/female.png';
import Button from './common/Button.component';
import PupilFormModal from './pupilFormModal.component';
import ToggleContent from './common/ToggleContent.component';
import { pupilPropTypes } from './propTypes';

const avatars = {
  male,
  female,
};

const Wrapper = styled.div`
  border: 1px solid ${props => props.theme.color.primary.default};
  color: ${props => props.theme.color.primary.default};
  padding: 2rem;
  display: grid;
  grid-template-columns: 1fr;
  justify-items: center;
  align-items: center;
  grid-gap: 1rem;
  border-radius: ${props => props.theme.globalStyle.radius};
  background-color: ${props => props.theme.color.grey.lighter};
`;

const Avatar = styled.img`
  vertical-align: middle;
  width: 50px;
  height: 50px;
  border-radius: 50%;
`;

const ButtonsWrapper = styled.div`
  display: inline-grid;
  grid-auto-flow: column;
  grid-gap: 10px;
  padding-top: 1rem;  
  svg>path{
    fill: ${props => props.theme.color.primary.default};
  }
`;

const PupilCard = ({ pupil, editPupil, removePupil }) => {
  const handleRemove = () => {
    removePupil(pupil);
  };
  return (
    <Wrapper>
      <Avatar src={avatars[pupil.gender]} />
      <div>
        {pupil.firstname} {pupil.lastname}
      </div>
      <ButtonsWrapper>
        <ToggleContent
          toggle={show => (
            <Button primary onClick={show}>
          Éditer
            </Button>
          )}
          content={hide => (
            <PupilFormModal
              title="Éditer un élève"
              onSubmit={(newPupil) => { editPupil(newPupil); hide(); }}
              isOpen
              onClose={hide}
              initialValues={pupil}
            />
          )}
        />
        <Button isStroked onClick={handleRemove}>
        Supprimer
        </Button>
      </ButtonsWrapper>
    </Wrapper>
  );
};

export default PupilCard;


PupilCard.propTypes = {
  pupil: PropTypes.shape(pupilPropTypes).isRequired,
  editPupil: PropTypes.func.isRequired,
  removePupil: PropTypes.func.isRequired,
};

PupilCard.defaultProps = {
};
