import React from 'react';
import { shallow } from 'enzyme';
import data from '../../data/pupils';
import PupilFormModal from '../pupilFormModal.component';

describe('PupilFormModal', () => {
  let props;

  beforeEach(() => {
    props = {
      onClose: jest.fn(),
      onSubmit: jest.fn(),
    };
  });

  const getWrapper = () => shallow(<PupilFormModal {...props}>Click</PupilFormModal>);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with initial values', () => {
    const firstPupil = data[0];
    // Given
    props.title = 'Title';
    props.initialValues = firstPupil;

    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
