import React from 'react';
import { shallow } from 'enzyme';
import data from '../../data/pupils';
import PupilForm from '../pupilForm.component';

describe('PupilForm', () => {
  let props;

  beforeEach(() => {
    props = {
      onSubmit: jest.fn(),
    };
  });

  const getWrapper = () => shallow(<PupilForm {...props}>Click</PupilForm>);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
  it('should render with initial values', () => {
    const firstPupil = data[0];
    // Given
    props.title = 'Title';
    props.initialValues = firstPupil;

    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
