import React from 'react';
import { shallow } from 'enzyme';
import data from '../../data/pupils';
import Pupil from '../pupil.component';

describe('Pupil', () => {
  let props;
  const firstPupil = data[0];

  beforeEach(() => {
    props = {
      pupil: firstPupil,
      editPupil: jest.fn(),
      removePupil: jest.fn(),
    };
  });

  const getWrapper = () => shallow(<Pupil {...props}>Click</Pupil>);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with initial values', () => {
    const firstPupil = data[0];
    // Given
    props.title = 'Title';
    props.initialValues = firstPupil;

    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
