import React from 'react';
import { shallow } from 'enzyme';
import data from '../../data/pupils';
import Classroom from '../classroom.component';

describe('Classroom', () => {
  let props;

  beforeEach(() => {
    props = {
      editPupil: jest.fn(),
      removePupil: jest.fn(),
      pupils: data,
    };
  });

  const getWrapper = () => shallow(<Classroom {...props}>Click</Classroom>);

  it('should render with default props', () => {
    // When
    const wrapper = getWrapper();

    // Then
    expect(wrapper).toMatchSnapshot();
  });
});
