import React, { useState } from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import Input from './common/Input.component';
import Button from './common/Button.component';
import Select from './common/Select.component';

import { pupilPropTypes } from './propTypes';


const Wrapper = styled.form`
  display: inline-grid;
  grid-gap: 2rem;
  align-content: flex-start;
  height: 100%;
  width: 100%;
  transition: all 1s;
`;

const genderOptions = [
  {
    label: 'Garçon',
    key: 'male',
  },
  {
    label: 'Fille',
    key: 'female',
  },
];


const PupilForm = ({ initialValues, onSubmit }) => {
  const [values, setValues] = useState(initialValues || { firstname: '', lastname: '', gender: 'male' });

  const handleChangeValue = (ev) => {
    ev.persist();
    setValues({ ...values, [ev.target.name]: ev.target.value });
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();
    onSubmit(values);
  };

  return (
    <Wrapper>
      <Input value={values.firstname} name="firstname" onChange={handleChangeValue} label="Prénom" />
      <Input value={values.lastname} name="lastname" onChange={handleChangeValue} label="Nom" />
      <Select label="Sexe" id="gender" options={genderOptions} onChange={handleChangeValue} value={values.gender} name="gender" />
      <Button type="submit" onClick={handleSubmit}>OK</Button>
    </Wrapper>
  );
};

export default PupilForm;

PupilForm.propTypes = {
  initialValues: PropTypes.shape(pupilPropTypes),
  onSubmit: PropTypes.func.isRequired,
};

PupilForm.defaultProps = {
  initialValues: null,
};
