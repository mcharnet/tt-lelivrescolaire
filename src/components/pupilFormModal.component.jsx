import React from 'react';
import PropTypes from 'prop-types';

import Modal from './common/Modal.component';
import PupilForm from './pupilForm.component';

import { pupilPropTypes } from './propTypes';

const PupilFormModal = ({ onSubmit, onClose, title, initialValues }) => (

  <Modal isOpen onClose={onClose} title={title}>
    <PupilForm onSubmit={onSubmit} initialValues={initialValues} />
  </Modal>

);


export default PupilFormModal;

PupilFormModal.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  initialValues: PropTypes.shape(pupilPropTypes),
};

PupilFormModal.defaultProps = {
  title: '',
  initialValues: null,
};
