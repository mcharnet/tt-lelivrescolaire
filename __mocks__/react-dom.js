/* eslint-disable import/no-extraneous-dependencies */

const React = require('react');
const ReactDOM = require('react-dom');

const Portal = () => {};

ReactDOM.createPortal = jest.fn(elem => React.createElement(Portal, {}, elem));

module.exports = ReactDOM;
